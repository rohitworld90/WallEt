package businessParty;

/**
 * @author rohsingh
 *
 *         This class defines any party that a User can deal with! Anything that
 *         the user can deal with is a Party
 */
public class BusinessParty {

    String partyID;
    String partyName;
    String partyContactNumber;
    String partyAddress;

    public String getPartyAddress() {
	return partyAddress;
    }

    public void setPartyAddress(String partyAddress) {
	this.partyAddress = partyAddress;
    }

    public String getPartyContactNumber() {
	return partyContactNumber;
    }

    public void setPartyContactNumber(String partyContactNumber) {
	this.partyContactNumber = partyContactNumber;
    }

    public String getPartyID() {
	return partyID;
    }

    public void setPartyID(String partyID) {
	this.partyID = partyID;
    }

    public String getPartyName() {
	return partyName;
    }

    public void setPartyName(String partyName) {
	this.partyName = partyName;
    }

 

}
