package input;

import java.util.Scanner;

import mongoManager.MongoDBOperations;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import util.BaseUtilities;
import businessParty.BusinessParty;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;

import entry.EntryClass;

public class InputHandler {
    Logger logger = LoggerFactory.getLogger(EntryClass.class.getName());

    /**
     * To welcome the user
     */

    public void welcomeUser() {
	logger.info("Hey! Welcome to WallEt");
	logger.info("Enter your log for today!");
    }

    /**
     * To get the user input
     * 
     * @param consoleScan
     */

    public void gatherUserInput(Scanner consoleScan) {
	boolean moreEntryLogs = false;
	MongoDBOperations local = new MongoDBOperations("localhost", "WallEt");
	MongoCollection<Document> mcd = local.readCollectionAsDoc("Party");
	FindIterable<Document> fi = mcd.find();

	Double transactionValue;

	do {
	    logger.info("Select your party and then enter your transaction details with that party");

	    logger.info(fi.toString());

	    logger.info("Enter party ID for the party you selected");
	    String tempPartyID = consoleScan.nextLine();
	    logger.info("Enter your description");
	    String tempTransactionDesc = consoleScan.nextLine();
	    logger.info("Enter your transaction");
	    transactionValue = consoleScan.nextDouble();
	    // To eatup NewLine char
	    consoleScan.nextLine();
	    Document doc = new Document();
	    doc.append("PartyID", tempPartyID)
		    .append("Transaction Description", tempTransactionDesc)
		    .append("Transaction Value", transactionValue);

	    local.insertDoc(doc, "Transactions");
	    logger.info("Do you want to enter more logs? true/false");
	    moreEntryLogs = consoleScan.nextBoolean();
	    // To eatup NewLine char
	    consoleScan.nextLine();

	} while (moreEntryLogs == true);

    }

    /**
     * To get the user input
     * 
     * @param consoleScan
     */

    public void addNewParty(Scanner consoleScan) {

	BusinessParty bp = new BusinessParty();

	logger.info("Enter party name: ");
	bp.setPartyName(consoleScan.nextLine());

	logger.info("Enter party Phone/Mobile Number: ");
	bp.setPartyContactNumber(consoleScan.nextLine());

	logger.info("Enter party Address: ");
	bp.setPartyAddress(consoleScan.nextLine());

	bp.setPartyID(BaseUtilities.generateNewPartyID());

	Document doc = new Document();

	doc.append("MobileNumber", bp.getPartyContactNumber())
		.append("Name", bp.getPartyName())
		.append("Address", bp.getPartyAddress())
		.append("ID", bp.getPartyID());
	MongoDBOperations local = new MongoDBOperations("localhost", "WallEt");
	local.insertDoc(doc, "Party");
	logger.info("Party added successfully!");

    }

    /**
 * 
 */
    public void viewWallEtStatus() {
	Double totalBal = 0.0;
	logger.info("Current Balance");

	MongoDBOperations local = new MongoDBOperations("localhost", "WallEt");
	// TODO implent
	logger.info("" + totalBal);

    }

}
