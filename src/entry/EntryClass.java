package entry;

import input.InputHandler;

import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EntryClass {

    public static void main(String[] args) {
	String mongoDBHost = "localhost";
	String mongoDBDatabaseName = "WallEt";
	//TODO Implement these variables

	Logger logger = LoggerFactory.getLogger(EntryClass.class.getName());
	Scanner consoleScan = null;
	try {
	    consoleScan = new Scanner(System.in);

	    InputHandler ih = new InputHandler();
	    boolean moreEntries = true;

	    ih.welcomeUser();
	    int choice;
	    do {

		logger.info("Enter your choice: ");
		logger.info("1 -> Add new Person ");
		logger.info("2 -> Enter your transaction logs ");
		logger.info("3 -> View status of your WallEt");
		logger.info("4 -> View your monthly expenses ");
		logger.info("5 -> View person wise balances ");

		choice = consoleScan.nextInt();
		// To consume next line character left!
		consoleScan.nextLine();
		switch (choice) {
		case 1:
		    logger.debug("Case 1 executed");
		    ih.addNewParty(consoleScan);
		    break;
		case 2:
		    logger.debug("Case 2 executed");
		    ih.gatherUserInput(consoleScan);
		    break;
		case 3:
		    logger.debug("Case 3 executed");
		    ih.viewWallEtStatus();
		    break;
		case 4:
		    logger.debug("Case 4 executed");
		    logger.info("Sorry! Feature not implemented yet ");
		    break;
		case 5:
		    logger.debug("Case 5 executed");
		    logger.info("Sorry! Feature not implemented yet ");
		    break;
		default:
		    logger.debug("Default case executed");
		    logger.info("Invalid input!");
		}
		logger.info("Do you want to continue? true/false");

		moreEntries = consoleScan.nextBoolean();
		// To consume next line character left!
		consoleScan.nextLine();

	    } while (moreEntries == true);
	} finally {
	    consoleScan.close();
	}
    }

}
