package mongoManager;

import java.util.ArrayList;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;

import entry.EntryClass;

/**
 * 
 * @author rohit
 *
 */
public class MongoDBOperations {
    Logger logger = LoggerFactory.getLogger(EntryClass.class.getName());

    MongoClient mongoClient;
    public MongoClient getMongoClient() {
        return mongoClient;
    }

    MongoDatabase db;

    public MongoDBOperations() {
	mongoClient = new MongoClient();
	logger.info("Local mongo connected");
	db = mongoClient.getDatabase("test");
	if (!dbExists("test")) {

	}

	logger.info("test DB connected");
    }

    /**
     * @param DBHost
     * @param DBName
     */
    public MongoDBOperations(String DBHost, String DBName) {
	mongoClient = new MongoClient(DBHost);
	logger.info("Connected mongo hostname: " + DBHost);
	db = mongoClient.getDatabase(DBName);
	logger.info("Connected mongo database: " + DBName);
    }

    /**
     * @param collectionName
     * @param fieldName
     * @param value
     *            This method adds a field with String value to a specified
     *            collection name
     */

    public void insertDoc(Document doc, String collectionName) {

	if (!collectionExists(collectionName)) {
	    db.createCollection(collectionName);
	    logger.info("Collection created: " + collectionName);
	}
	db.getCollection(collectionName).insertOne(doc);
	;

    }

    /**
     * 
     * @param doc
     *            To read complete collection as a document
     * @param collectionName
     */
    public MongoCollection<Document> readCollectionAsDoc(String collectionName) {

	if (!collectionExists(collectionName)) {
	    logger.error("Trying to read from a non existing collection");
	}
	return db.getCollection(collectionName);

    }

    /**
     * @param CollectionName
     * @return This method checks if the specified collection exists in
     *         currently specified database
     */
    public boolean collectionExists(String CollectionName) {
	MongoIterable<String> collectionList = db.listCollectionNames();
	for (String tempCollectionList : collectionList) {
	    if (tempCollectionList.equals(CollectionName)) {
		return true;
	    }
	}
	return false;
    }

    /**
     * @param dbName
     * @return
     */
    public boolean dbExists(String dbName) {
	MongoIterable<String> dbList = mongoClient.listDatabaseNames();
	for (String dbListTemp : dbList) {
	    if (dbListTemp.equals(dbName)) {
		return true;
	    }
	}
	return false;
    }

}
